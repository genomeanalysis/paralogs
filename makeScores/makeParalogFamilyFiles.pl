#!/usr/bin/perl
#
use strict;

my $paralogdefinitionfile="paralogs.ensemble.txt";
my $idfile="name.conversion.txt";

my $verbose = 0;

my %ens2hgnc=();
open(IDS,$idfile) or die $!;
while(my $str=<IDS>){
	chomp($str);
	next if $str!~/\S/ or $str=~/Ensembl/;
	my ($geneid,$transid,$protid,$hgnc)=split(/\t/,$str);
	next if length($hgnc) eq 0 or length($protid) eq 0;
	if($hgnc){
		$ens2hgnc{$geneid}=$hgnc;
	}else{
		$ens2hgnc{$geneid}=$protid;
	}
}
close(IDS);

my %found=();
my %groups=();
my $paragroups=1;
open(FILE,$paralogdefinitionfile) or die $!;
while(my $str=<FILE>){
	chomp($str);
	next if $str!~/\S/ or $str=~/Ensembl/;
	my ($geneid,$transid,$paralogid)=split(/\t/,$str);
	
	next if $paralogid!~/\S/;

	my $ngeneid=$geneid;
	if(exists($ens2hgnc{$geneid})){
		$ngeneid=$ens2hgnc{$geneid};
	}else{
		print STDERR "# GENE missing: $geneid\n" if $verbose;
	}

	
	my $nparalogid=$paralogid;
	if (exists($ens2hgnc{$paralogid})){
		$nparalogid=$ens2hgnc{$paralogid};
	}else{
		print STDERR "# PARALOG: $paralogid\n" if $verbose;
	}
	
	if(!exists($found{$ngeneid})){
		$groups{$paragroups}{$ngeneid}++;
		$found{$ngeneid}=$paragroups;
		$paragroups++;
	}

	$groups{$found{$ngeneid}}{$nparalogid}++;
	$found{$nparalogid}=$found{$ngeneid};
}
close(FILE);

my $ct=1;
print "GROUPID\tno\tids\n";
foreach my $id (keys(%groups)){
	print $ct++;
	my @ids=keys(%{$groups{$id}});
	print "\t".scalar(@ids);
	print "\t".join(",",@ids);
	print "\n";
}
