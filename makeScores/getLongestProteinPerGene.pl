#!/usr/bin/perl
#
use strict;

my $fasta="CCDS_protein.current.faa";
my %seqs=();
my ($id);
open(FASTA,$fasta) or die $!;
while(my $str=<FASTA>){
	chomp($str);
	next if $str!~/\S/;
	next if $str=~/ccds_status/;
	if($str=~/>(.+)$/){
		my $fastaline=$1;
		my ($ccds,$hs,$chr)=split(/\|/,$fastaline);
		$id=$ccds;
	}else{
		$seqs{$id}.=$str;
	}
}
close(FASTA);

my %genes=();
my $ccds="CCDS.20150512.public.txt";
my %ccds2refseq=();
open(CCDS,$ccds) or die $!;
while(my $str=<CCDS>){
	chomp($str);
	next if $str=~/ccds_status/;
	next if $str!~/\S/;
	my @split=split(/\t/,$str);
	$genes{$split[2]}{$split[4]}++;
}

open(IDS,">CCDS_IDS.txt");
foreach $id (keys(%genes)){
	my $size=-1;
	my $canonid="";
	foreach my $ccdsid (keys(%{$genes{$id}})){
		if (exists($seqs{$ccdsid})){
			if (length($seqs{$ccdsid})>$size){
				$canonid=$ccdsid;
				$size=length($seqs{$ccdsid});
			}
		}else{
			print STDERR "# CCDS id $ccdsid does not exist\n";
		}
	}
	print IDS "$id\t$canonid\n";
	print ">$id\n".$seqs{$canonid}."\n";
}
close(CCDS);
close(IDS);
