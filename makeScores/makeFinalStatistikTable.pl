#!/usr/bin/perl

use strict;

my (%genes);

my $paralogfile="paraloggroups.HGNC.CCDS.tsv";
open(PARA,$paralogfile) or die $!;
while(my $str=<PARA>){
        chomp($str);
        next if $str!~/\S/;
	my ($group,$nrpara,$genes)=split(/\t/,$str);
	my @genes=split(/,/,$genes);
	foreach my $gene (@genes){
		$genes{$gene}{'paraloggroup'}=$group;
		my @paralogs=subtractEntryFromArray(\@genes,$gene);
		$genes{$gene}{'paralogs'}=join(",",@paralogs);
		$genes{$gene}{'paralogsno'}=scalar(@paralogs);
	}
}	
close(PARA);

my $groupfile="allgroups.detailed.tsv";

print "GENE\tnr_paralogs\tparalogs\tparaloggroup_id\tsubgroup_id\tnr_of_paralogs_in_sg\tparalogs_in_sg\tnr_of_nonhuman_homolog_genes_in_sg\tnonhuman_homolog_genes_in_sg\n";

open(FILE,$groupfile) or die $!;
while(my $str=<FILE>){
	chomp($str);
	next if $str!~/\S/;
	my ($group,$nrtotal,$nrhuman,$nrnonhuman,$human,$nonhuman)=split(/\t/,$str);
	my @humangenes=split(/,/,$human);
	my @nonhumangenes=split(/,/,$nonhuman);
	foreach my $gene (@humangenes){
		print $gene;
		print "\t".$genes{$gene}{'paralogsno'};
		print "\t".$genes{$gene}{'paralogs'};
		print "\t".$genes{$gene}{'paraloggroup'};
		print "\t".$group;
		print "\t".($nrhuman-1);
		print "\t".join(",",subtractEntryFromArray(\@humangenes,$gene));
		my @homologs=getHomologsFromArray(\@nonhumangenes,$gene);
		print "\t".scalar(@homologs);
		print "\t".join(",",@homologs);
		print "\n";
	}
}
close(FILE);

sub getHomologsFromArray{
	my @genes=@{$_[0]};
	my $gene=$_[1];

	my @newgenes=();
	foreach my $g (@genes){
		push @newgenes, $g if $g=~/^$gene\|/;
	}
	return @newgenes;
}
sub subtractEntryFromArray{
	my @genes=@{$_[0]};
	my $gene=$_[1];

	my @newgenes=();
	foreach my $g (@genes){
		push @newgenes, $g if $g ne $gene;
	}
	return @newgenes;
}

