#!/usr/bin/perl
#
use strict;

my $fasta="canonical.faa";

my %seqs=();
my ($id);
open(FASTA,$fasta) or die $!;
while(my $str=<FASTA>){
    chomp($str);
    next if $str!~/\S/;
    if($str=~/>(.+)$/){
	$id=$1;
    }else{
	$seqs{$id}.=$str;
    }
}
close(FASTA);

my $groups="paralogs.groups.tsv";
open(GROUP,$groups) or die $!;
while(my $str=<GROUP>){
    chomp($str);
    next if $str!~/\S/;
    next if $str=~/GROUPID/;
    my ($groupid,$members,$genes)=split(/\t/,$str);
    my @genes=split(/,/,$genes);
    my $seqs="";
    my $ct=0;
    foreach my $gene (@genes){
	if(exists($seqs{$gene})){
	    $seqs.=">$gene\n$seqs{$gene}\n";
	    $ct++;
	}else{
	    print STDERR "# ERROR: $gene sequence not available\n";
	}
    }
    if($ct>2){
      #print STDERR "# OK: group $groupid $ct member:$genes\n";
#	my $newfasta="paraloggroups/".$groupid.".fasta";
#	open(NEW,">$newfasta") or die $!;
#	print NEW $seqs;
#	close(NEW);
    }else{
	next if $ct eq 0;
        print STDERR "# ERROR: less than 2 sequences: $ct\n";
	my $newfasta="smallgroups/".$groupid.".fasta";
	open(NEW,">$newfasta") or die $!;
	print NEW $seqs;
	close(NEW);
    }
}
close(GROUP);



