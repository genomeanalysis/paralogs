#!/usr/bin/perl

use strict;
use Data::Dump qw(dump);
use Graph::Undirected::Components;

my $clwfile=$ARGV[0];
my $fastafile=$ARGV[1];

$fastafile=~/(\d+).fasta/;
my $tail=$1;

my %uid2ids=();

my %seqs=();
my ($seqid);
open(FASTA,$fastafile) or die $!;
while(my $str=<FASTA>){
    chomp($str);
    next if $str!~/\S/;
    if($str=~/>(.+)$/){
	$seqid=$1;
	if($seqid =~/\|/){
		my ($gid,$uid,$co,$spe)=split(/\|/,$seqid);
		print STDERR "# SEQID: $seqid\n";
		die "# ERROR: $uid $seqid already exists\n" if exists($uid2ids{$gid."|".$uid});
		$uid2ids{$gid."|".$uid}=$seqid;	
		print STDERR "# ID: $gid|$uid $seqid\n";
	}
    }else{
	$seqs{$seqid}.=$str;
    }
}
close(FASTA);

my $thres=0.8;
$thres=$ARGV[2] if $ARGV[2];

my %alignments=();
open(CLW,$clwfile) or die $!;
while( my $str=<CLW>){
	chomp($str);
	next if $str!~/\S/;
	next if $str=~/MUSCLE/;
	my ($id,$align)=split(/[\s\t]+/,$str);
	next if $id !~/\S/;
	$alignments{$id}.=$align;
}
close(CLW);
my @ids=keys(%alignments);

print STDERR "# number of ids:".scalar(@ids)."\n";
my $alignlength=length($alignments{$ids[0]});

my %cluster=();
my %gene2cluster=();
my $curclstr=0;

my %percentages=();
my $componenter = Graph::Undirected::Components->new();
my %vertices=();
my %vertice2nr=();
my $vct=1;
print "#ID1\tID2\tAlignlength\taligned\tunaligned\talignedPercentage\n";
for(my $i=0;$i<scalar(@ids)-1;$i++){
	my $id1=$ids[$i];
	my $align1=$alignments{$id1};
	for(my $j=$i+1;$j<scalar(@ids);$j++){
		my $alignedct=0;
		my $unalignedct=0;
		my $alignlen=0;
		my $id2=$ids[$j];
		my $align2=$alignments{$id2};
		for (my $k=0;$k<$alignlength;$k++){
			my $substr1=substr($align1,$k,1);
			my $substr2=substr($align2,$k,1);
			next if $substr1 eq "-" and $substr2 eq "-";
			if($substr1 ne "-" and $substr2 ne "-"){
				$alignedct++;
			}else{
				$unalignedct++;
			}
			$alignlen++;
		}
		my $percent=sprintf("%.2f",$alignedct/$alignlen);
		print "$id1\t$id2\t$alignlen\t$alignedct\t$unalignedct\t$percent\n";
		next if $percent < $thres;
		$percentages{$percent}{$id1."_".$id2}++;
		if(!exists($vertices{$id1})){
		    $vertices{$id1}=$vct;
		    $vertice2nr{$vct}=$id1;
		    $vct++;
		}
		if(!exists($vertices{$id2})){
		    $vertices{$id2}=$vct;
		    $vertice2nr{$vct}=$id2;
		    $vct++;
		}
		$componenter->add_edge($vertices{$id1}, $vertices{$id2});
      }
}	

my @subgraphs= $componenter->connected_components ();
for my $component (0 .. @subgraphs - 1) {
    my @vertices=@{ $subgraphs[$component] };
    my $clust=$component+1;
    print STDERR "# COMPONENT: $component\n";
    foreach my $v (@vertices){
	print STDERR "# vertex: $v  $vertice2nr{$v}\n";
	$cluster{$clust}{$vertice2nr{$v}}++;
    }
}

# cluster
#
my $clustfile="$tail.clstr";
open(CLUST,">$clustfile") or die $!;

foreach my $clust (keys(%cluster)){
    my $newfile="$tail.$clust.fasta";
    my $clustersize=scalar(keys(%{$cluster{$clust}}));
    next if $clustersize < 2;
    my $newcluster="$tail.$clust\t";
    my $newfasta;
    my $ct=0;
    my $ok=0;

    foreach my $id (keys(%{$cluster{$clust}})){
	next if $id!~/\S/;
        $newcluster.= "," if $ct>0;
	$newcluster.=$id;
	$ok=1 if $id !~/\|/;

	$id=~s/\|ch\w*$/\|chimpanzee/;
	$id=~s/\|ma\w*$/\|macaque/;
	$id=~s/\|ca\w*$/\|cattle/;
	$id=~s/\|mo\w*$/\|mouse/;
	$id=~s/\|d\w*$/\|dog/;
	$id=~s/\|r\w*$/\|rat/;

	if ($id=~/\|/){
		my @ids=split(/\|/,$id);
		$id=$uid2ids{$ids[0]."|".$ids[1]} if exists($uid2ids{$ids[0]."|".$ids[1]});
		print STDERR "# ID ERROR: $ids[0]|$ids[1] id does not exist in fasta\n" if !exists($uid2ids{$ids[0]."|".$ids[1]});
	}

	if(exists($seqs{$id})){
	    $newfasta.= ">$id\n$seqs{$id}\n";
	}else{
	    print STDERR "# ERROR: seq $id not found\n";
	}
	$ct++;
    }
    if ($ok){
	open(NEW,">$newfile") or die $!;
	print NEW $newfasta if $ok;
	print CLUST "$newcluster\n" if $ok;
	close(NEW);
    }else{
	print "# ERROR: cluster without human seqs: $newcluster\n"; 
    }

}
close(CLUST);
