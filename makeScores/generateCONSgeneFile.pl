#!/usr/bin/perl
#
use strict;

my $alignfile=$ARGV[0];
my $scorefile=$ARGV[1];

my $idfile=$alignfile;
$idfile=~s/aln/ids/;

my @scores=();
open(SCORE,$scorefile) or die $!;
while(my $str=<SCORE>){
	chomp($str);
	next if $str!~/\S/;
	$str=~s/,$//;
	@scores=split(/,/,$str);
	print STDERR "# SCORES:".scalar(@scores)."\n";
}
close(SCORE);

my %alignments=();
open(ALIGN,$alignfile) or die $!;
while(my $str=<ALIGN>){
	chomp($str);
	next if $str!~/\S/ or $str=~/^(\s|\t)/;
	my ($id,$align)=split(/[\s\t]+/,$str);
	next if $align!~/\S/;
	$alignments{$id}.=$align;
}
close(ALIGN);

open(IDS,">$idfile") or die $!;

foreach my $id (keys(%alignments)){

	my $align=$alignments{$id};
	my ($gene,$up,$com,$species)=split(/\|/,$id);
	print STDERR "# $id length=".length($align)."\n";
	print STDERR "ERROR: score length=".scalar(@scores)." is not equal align length: ".length($align)."\n" if length($align) ne scalar(@scores);
	my $file=$gene.".txt";
	my $statfile=$gene.".stat.txt";

	next if $up=~/\S/;

	print IDS "$id\n";
	print STDERR  "# NEW FILE: $file\n";
	open(NEW,">$file") or die $!;
	my $aapos=0;
	my %convscores=();
	my $totalscore=0;
	for(my $i=0;$i<length($align);$i++){
		my $cur=substr($align,$i,1);
		next if $cur eq "-";
		print NEW ++$aapos."\t".$cur."\t".$scores[$i]."\n";
		$convscores{int($scores[$i])}++;
		$totalscore+=$scores[$i];
	}
	close(NEW);
	
	open(NEWSTAT,">$statfile") or die $!;
	print NEWSTAT "# GENE: $gene\n";
	print NEWSTAT "# LENGTH: $aapos\n";
	print NEWSTAT "# TOTALSCORE: $totalscore\n";
	foreach ( 0 .. 11) {
		my $score=0 if !exists($convscores{$_});
		$score=$convscores{$_} if exists($convscores{$_});
		print NEWSTAT "# SCORE $_ $score\n";
	}

	close(NEWSTAT);
}
close(ALIGN);
close(IDS);
