#!/bin/bash

id=$1

aln=$id.aln
out=$id.cons.scores

tail -n +4 $id.clw >$aln

jalviewdir=/Applications/Jalview

groovy=conservation.groovy

java -Djava.ext.dirs=$jalviewdir/lib -cp $jalviewdir/jalview.jar jalview.bin.Jalview nodisplay -open $aln -groovy $groovy | tail -n +6 >$out

