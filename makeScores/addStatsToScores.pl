#!/usr/bin/perl
#
use strict;

my $ids=$ARGV[0];
my $dir=$ARGV[1];

my %ids=();
open(IDS,$ids) or die $!;
while(my $str=<IDS>){
    chomp($str);
    next if $str!~/\S/;
    $ids{$str}++;
}
close(IDS);

foreach my $gene (keys(%ids)){
    my $file=$dir."/$gene.txt";
    my $newfile=$dir."/$gene.withStats.txt";
    
    my ($totalscore,$len,$paraScoreGreater0,$paraScore11,$mean,$std,$median,$meanGreater0,$stdGreater0,$medianGreater0,$pscores,$pscoresGreater0,$lines) = getScores($file);
    my @scores=@{$pscores};
    my @scoresGreater0=@{$pscoresGreater0};
    my @lines=@{$lines};

    print STDERR "# WRITE new file $newfile\n";
    open(NEW,">$newfile") or die $!;
    print NEW "# GENE: $gene\n# TOTALSCORE=$totalscore\n# LEN=$len\n# MEAN(SCORE)=$mean\n# STD(SCORE)=$std\n# MEDIAN(SCORE)=$median\n# SCORE>0=$paraScoreGreater0\n# MEAN(GREATER>0)=$meanGreater0\n# STD(SCORE>0)=$stdGreater0\n# MEDIAN(SCORE>0)=$medianGreater0\n# MAXSCORES=$paraScore11\n";
    print STDERR "# GENE: $gene\n# TOTALSCORE=$totalscore\n# LEN=$len\n# MEAN(SCORE)=$mean\n# STD(SCORE)=$std\n# MEDIAN(SCORE)=$median\n# SCORE>0=$paraScoreGreater0\n# MEAN(GREATER>0)=$meanGreater0\n# STD(SCORE>0)=$stdGreater0\n# MEDIAN(SCORE>0)=$medianGreater0\n# MAXSCORES=$paraScore11\n\n";
    print NEW "#POS\tAA\tSCORE\tSCORE-MEDIAN\t(SCORE-MEDIAN)/STD\tSCORE-MEAN\t(SCORE-MEAN)/STD\n";
  
    if($std < 0.001){ 
	$std=0.0001;
	print STDERR "# CHECK: STD $std is zero\n";
    }	
    foreach my $line (@lines){
	my ($pos,$aa,$score)=split(/\t/,$line);
	print NEW "$pos\t$aa\t$score\t".($score-$median)."\t".sprintf("%.2f",($score-$median)/$std)."\t".($score-$mean)."\t".sprintf("%.2f",($score-$mean)/$std)."\n";
    }
    close(NEW);
}

# getMeanStd calculates the mean and the standard deviation 
# for given array

sub getMeanStd{

    my @values = @{$_[0]};
    my $n      = scalar(@values);

    my $mean = 0;
    my $std  = 0;

    return (0,0) if $n==0;

    foreach(@values){
	$mean+=$_;
    }
    $mean/=$n;

    foreach(@values){
	$std+=($_-$mean)*($_-$mean);
    }
    if ($n>1) {
	$std /= ($n-1);
	$std  = sqrt($std);
    }else{
	$std=0;
    }
    return ($mean,$std);
}

sub median{

    my @Array = sort({$a<=>$b} @{$_[0]});

    my $median;

    if( (@Array % 2) == 1 ) {
	$median = $Array[((@Array+1) / 2)-1];
    } else {
	$median = ($Array[(@Array / 2)-1] + $Array[@Array / 2]) / 2;
    }

    return $median;
}

sub getScores{

    my $genescorefile=$_[0];
    my $len=0;
    my $totalscore=0;
    my $paraScoreGreater0=0;
    my $paraScore11=0;
    my @scores=();
    my @scoresGreater0=();
    my @lines=();

    open(SCORE,$genescorefile) or die "# ERROR: SCOREFILE: $genescorefile not found\n";
    while(my $str=<SCORE>){
	chomp($str);
	next if $str!~/\S/;
	my ($aapos,$aa,$consscore)=split(/\t/,$str);
	$len=$aapos;
	$totalscore+=$consscore;
	push @scores,$consscore;
	push @scoresGreater0, $consscore if $consscore>0;
	push @lines,$str;

	$paraScoreGreater0++ if $consscore>0;
	$paraScore11++ if $consscore eq 11;
    }
    close(SCORE);

    print STDERR "# FILE=$genescorefile\n# LEN=$len greater0=$paraScoreGreater0 equal11=$paraScore11\n";
    $paraScoreGreater0=sprintf("%.2f",($paraScoreGreater0/$len));
    $paraScore11=sprintf("%.2f",($paraScore11/$len));
    my ($mean,$std)=getMeanStd(\@scores);
    my ($meanGreater0,$stdGreater0)=getMeanStd(\@scoresGreater0);
    my $median=median(\@scores);
    my $medianGreater0=median(\@scoresGreater0);

    return ($totalscore,$len,$paraScoreGreater0,$paraScore11,sprintf("%.2f",$mean),sprintf("%.2f",$std),$median,sprintf("%.2f",$meanGreater0),sprintf("%.2f",$stdGreater0),$medianGreater0,\@scores,\@scoresGreater0,\@lines);
}


