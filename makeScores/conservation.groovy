def alignFrame;
  try {
  	alignFrame = currentAlFrame; // when in headless mode
	  } catch (e) {
        // must not be in headless mode
		alignFrame = Jalview.getAlignframes()[0];
	  };
	  
    def alignment = alignFrame.viewport.alignment; // jalview.datamodel.Alignment
    for (aa in alignment.getAlignmentAnnotation())
    {
        // aa is jalview.datamodel.AlignmentAnnotation
        if (aa.label == "Conservation")
        {
            // print consensus values
            for (ann in aa.annotations) // jalview.datamodel.Annotation
            {
                print ann.value.toInteger() + ","
            }
            print "\n";
        }
}
