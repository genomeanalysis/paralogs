#!/bin/bash

muscle=muscle
fastadir=$1
aligndir=$2

for i in `cut -f 1 paralogs.groups.tsv`
do
	$muscle -in $fastadir/$i.fasta -out $aligndir/$i.clw -clw
done
