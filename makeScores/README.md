# Files to download to this directory:
All files neccessary to run the paralog annotation tool locally can be downloaded from the Zenodo repository [Paralog variant classification and scoring](https://zenodo.org/record/840860)

## Download Annotation files
```
wget https://zenodo.org/record/840860/files/CCDS.20150512.public.txt
wget https://zenodo.org/record/840860/files/CCDS_protein.current.faa
wget https://zenodo.org/record/840860/files/paralogs.ensemble.txt
wget https://zenodo.org/record/840860/files/name.conversion.txt

```

## Generate fasta with canonincal CCDS sequences
This script uses the current CCDS download file `CCDS.20150512.public.txt` with their corresponding fasta sequences to generate a new file `CCDS_IDS.txt` with the IDs of longest CCDS transcripts per gene  
and prints out the corresponding fasta sequence.

```
perl getLongestProteinPerGene.pl >canonical.faa 
```

## Generate paralog groups

# Input files

* paralogs.ensemble.txt: paralog definitions from Ensembl  
* name.conversion.txt: name conversion Ensembl identifier to HGNC
* canonical.faa: fasta sequences of canonical CCDS isoforms

```
perl makeParalogFamilyFiles.pl >paralogs.groups.tsv
mkdir paraloggroups
perl makeParalogFamilySeqs.pl 2>err.log
```

## make alignments

* assumes that you have installed [muscle](http://www.drive5.com/muscle)  

```
./runMuscleAlignment.sh paraloggroups paraloggroups
```

## make clustering and run Jalview

* Perl modules [Data::dump](http://search.cpan.org/~gaas/Data-Dump-1.23/lib/Data/Dump.pm) and [Graph::Undirected::Components](http://search.cpan.org/~kubina/Graph-Undirected-Components-0.31/lib/Graph/Undirected/Components.pm) should be installed
* [muscle](http://www.drive5.com/muscle) should be in your PATH
* [Jalview](http://www.jalview.org) must be installed  
  - for JalView a JDK has to be installed
  - please change paths to Jalview lib and jalview.jar in `getScoresFromJalview.sh`
  - the [Groovy](http://groovy-lang.org/) script `conservation.groovy` should be in the directory (or set path to it in `getScoresFromJalview.sh`)
* `getScoresFromJalview.sh` takes an `family_id` as input and generates two files as output
  - `family_id`.aln: the actual input file for [Jalview](http://www.jalview.org) 
  - `family_id`.cons.scores: conservation scores from [Jalview](http://www.jalview.org) for every position of the family alignment
* `generateCONSgeneFile.pl` takes the .aln and .cons.scores files as input and generates for every gene a `geneid`.txt file with the parascore per position as well as file `geneid`.stat.txt with some summary statistics on the parascore for this gene. Additionally a file `family_id`.ids is generated with the gene ids from every gene within the processes alignment.
* `addStatsToScores.pl` takes the file with the gene ids and a directory name as input, where the final score files should be stored (here:`scores`)
* the final output for each gene is a file `geneid`.withStats.txt

```
mkdir paralogsubgroups
cd paralogsubgroups
mkdir scores
for i in `cut -f 1 ../paralogs.groups.tsv`
do
	perl ../makeClustering.pl ../paraloggroups/$i.clw ../paraloggroups/$i.fasta	 
	for id in `cat $i.clstr | cut -f 1 `
	do
		muscle -in $id.fasta -out $id.clw -clw
		echo $id >>paralogsub.ids
		../getScoresFromJalview.sh $id 
		perl ../generateCONSgeneFile.pl $id.aln $id.cons.scores
		perl ../addStatsToScores.pl $id.ids scores
	done
done
cd ..
```

## Output

An example output file can be found here [../example/example.output](../example/example.output).

```
REF_ENS_GENES   AACHANGE_SCORED GENE    PARASCORE       PARASCOREzSCORE PARAFAM_ID      PARAFAM_SIZE    PARAFAM_GENES
GABRP;ENSG00000094755;ENSG00000094755   ENSG00000094755:ENST00000265294:exon10:c.1208delT:p.V403fs      GABRP   2       -0.80   3268.para.1     14      GLRA2,GABRR1,GLRB,GLRA1,GABRB1,GABRD,GABRR3,GABRP,GLRA4,GABRB3,GABRB2,GABRQ,GABRR2,GLRA3
HCN1;ENSG00000164588    HCN1:NM_021072:exon8:c.G2561A:p.R854Q   HCN1    0       -0.87   933.para.1      16      KCNH6,KCNH3,KCNH5,HCN3,KCNH4,HCN2,CNGA2,CNGA1,HCN1,KCNH2,KCNH7,CNGA4,CNGA3,CNGB3,KCNH1,KCNH8
HCN2;ENSG00000099822    ENSG00000099822:ENST00000251287:exon2:c.T782C:p.L261S   HCN2    8       1.66    933.para.1      16      KCNH6,KCNH3,KCNH5,HCN3,KCNH4,HCN2,CNGA2,CNGA1,HCN1,KCNH2,KCNH7,CNGA4,CNGA3,CNGB3,KCNH1,KCNH8

```

## Output

* Output format for `geneid`.withStats file (example can be found under [../example/STK17B.withStats.txt](../example/STK17B.withStats.txt)

```
# GENE: STK17B
# TOTALSCORE=2795
# LEN=372
# MEAN(SCORE)=7.51
# STD(SCORE)=3.35
# MEDIAN(SCORE)=8
# SCORE>0=0.91
# MEAN(GREATER>0)=8.24
# STD(SCORE>0)=2.51
# MEDIAN(SCORE>0)=9
# MAXSCORES=0.31
#POS    AA      SCORE   SCORE-MEDIAN    (SCORE-MEDIAN)/STD      SCORE-MEAN      (SCORE-MEAN)/STD
1       M       0       -8      -2.39   -7.51   -2.24
2       S       0       -8      -2.39   -7.51   -2.24
3       R       0       -8      -2.39   -7.51   -2.24
4       R       0       -8      -2.39   -7.51   -2.24
..
363     P       0       -8      -2.39   -7.51   -2.24
364     H       0       -8      -2.39   -7.51   -2.24
365     E       0       -8      -2.39   -7.51   -2.24
366     L       0       -8      -2.39   -7.51   -2.24
367     V       0       -8      -2.39   -7.51   -2.24
368     S       0       -8      -2.39   -7.51   -2.24
369     D       0       -8      -2.39   -7.51   -2.24
370     L       0       -8      -2.39   -7.51   -2.24
371     L       0       -8      -2.39   -7.51   -2.24
372     C       0       -8      -2.39   -7.51   -2.24
```

* comment section (starting with `#`)
    - GENE: gene name
    - TOTALSCORE: sum of all scores (column 3)
    - LEN: number of residues
    - MEAN(SCORE): mean of score (column 3)
    - STD(SCORE):   std of score
    - MdEDIAN(SCORE): median of score
    - SCORE>0: percentage of residues with score> 0 
    - MEAN(GREATER>0): mean score of residues with score>0
    - STD(SCORE>0): std score of residues with score>0
    - MEDIAN(SCORE>0): median score of residues with score>0
    - MAXSCORES: percentage of residues with max conservation score (=11)
* per residue:


1. POS                  - residue position in protein  
2. AA                   - aa at position POS
3. SCORE                - Jalview conservation score at POS
4. SCORE-MEDIAN         - SCORE minus MEDIAN (over all positions)
5. (SCORE-MEDIAN)/STD   - 4 divided by standard deviation 
6. SCORE-MEAN           - SCORE minus MEAN
7. (SCORE-MEAN)/STD     - z-score
