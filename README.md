This repository includes all scripts and links to rebuild the construction of paralog annotation used in the paper [Lal, May, et al.](https://www.biorxiv.org/content/10.1101/159780v1). 
All necessary filed can be downloaded from the Zenodo repository [Paralog variant classification and scoring](http://www.doi.org/10.5281/zenodo.802847).

## [bin](bin) 
Script directory. Contains scripts for annotating exonic missense variants with the *parazscore*.

## [data](data) 
Contains input data to generate the *parazscore* for annotation that is used from the scripts in the [bin](bin) directory.

## [example](example) 
Contains example input and output files for the variant annotation scripts in [bin](bin).

## [makeScores](makeScores) 
Contains all scripts and files neccessary to run the paralog annotation workflow locally as used in used in the paper [Lal, May, et al.](https://www.biorxiv.org/content/10.1101/159780v1).
Necessary large data files can be downloaded from the Zenodo repository [Paralog variant classification and scoring](http://www.doi.org/10.5281/zenodo.802847).
