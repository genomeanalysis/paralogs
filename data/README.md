# Files to download to this directory:
All files neccessary to run the paralog annotation tool locally can be downloaded from the Zenodo repository [Paralog variant classification and scoring](https://zenodo.org/record/817898)

## Annotation files
```
wget https://zenodo.org/record/817898/files/CCDS_IDS.txt  
wget https://zenodo.org/record/817898/files/CCDS2Ensembl2HGNC.txt  
wget https://zenodo.org/record/817898/files/refSeqEnsCCDS.tsv  
```

# Download scores
## Para scores per gene 
````
wget https://zenodo.org/record/817898/files/PARAzSCORES.tar.gz  
tar -zxvf PARAzSCORES.tar.gz
```

After downloading and decompressing one directory for each of the thee different score types will be available in the ```data``` directory
```
para_zscores              # Directory with gene-specific files with parascores and para_zscores

```

# Directory and subdirectory structures

# 1. paraScores  

Subdirectories
* genes - contains for each gene a `genename`.txt, `genename`.stat.txt and `genename`.withStats.txt file (genenames can be found in `parasubGeneIds.txt`)  
* fasta - contains for each paralog subfamily a fasta file (subfamily numbers and descriptions can be found in the files `parasubFamilyIds.txt` and `parasubgroups.detailed.tsv`)  
* parasubfamilies - contains the alignment files in clustalW format from [MUSCLE](http://www.drive5.com/muscle/) and the conservation scoring from [JalView](http://www.jalview.org/) per paralog subfamily  

Files:
* PARAzSCORE.geneIds.txt - file with [HGNC](http://www.genenames.org/) gene names present in paralog subfamilies
* PARAzSCORE.genes.tsv - file with `para_zscore` statistics per gene
* PARAzSCORE.families.tsv - file with `para_zscore` statistics per paralog subfamily
* parasubfam.probs.tsv - file with [ExAC](http://exac.broadinstitute.org) probilities for missense, LoF and missense.plus.LoF variants per para subfamily and number of genes per family
* parasubFamilyIds.txt - paralog subfamily ids
* parasubgroups.detailed.tsv - file with detailed description of each paralog subfamily including subfamily-id, number of genes per subfamily and gene names

# File formats

## Gene files
For each of the three scores (para, parasub, parahomo) the `.geneIds.txt` file gives the genenames for which the according score is available.  

Per gene there are two diffferent files available in the according score directory":

* genes/`genename`.txt eg. genes/GRIN2A.txt 

score per aminoacid  
format: AA position AA  score
 ```
1   M	4
2	G	8
3	R	3
4	V	4
5	G	0
6	Y	5
7	W	6
8	T	4
..
```

* genes/`genename`.withStats.txt eg. genes/GRIN2A.withStats.txt 

score per aminio acid plus gene-specific metrices like median/median/standard deviation (STD) in the header and z-scores per position  
format:AA position AA score    score_minus_median  (score-median)/STD  score-mean  (score-mean)/STD
```                 
# GENE: GRIN2A
# TOTALSCORE=9060
# LEN=1464
# MEAN(SCORE)=6.19
# STD(SCORE)=4.04
# MEDIAN(SCORE)=7
# SCORE>0=0.80
# MEAN(GREATER>0)=7.70
# STD(SCORE>0)=2.95
# MEDIAN(SCORE>0)=8
# MAXSCORES=0.28
#POS	AA	SCORE	SCORE-MEDIAN	(SCORE-MEDIAN)/STD	SCORE-MEAN	(SCORE-MEAN)/STD
1	M	4	-3	-0.74	-2.19	-0.54
2	G	8	1	0.25	1.81	0.45
3	R	3	-4	-0.99	-3.19	-0.79
4	V	4	-3	-0.74	-2.19	-0.54
5	G	0	-7	-1.73	-6.19	-1.53
6	Y	5	-2	-0.50	-1.19	-0.29
7	W	6	-1	-0.25	-0.19	-0.05
8	T	4	-3	-0.74	-2.19	-0.54
..
```

## Parastatistics files

# Parascore statistics per gene

* genes/`genename`.stat.txt eg. genes/GRIN2A.stat.txt 

Parascore distribution for a given gene. Parascore is the conservation score as given by JalView, Length is gene length, Totalscore is the sum of all parascors 
over the length of the gene, SCORE 0 287 means that 287 position had the parascore 0


```
# GENE: GRIN2A
# LENGTH: 1464
# TOTALSCORE: 9060
# SCORE 0 287
# SCORE 1 1
# SCORE 2 24
# SCORE 3 81
# SCORE 4 126
# SCORE 5 127
# SCORE 6 81
# SCORE 7 110
# SCORE 8 98
# SCORE 9 106
# SCORE 10 18
# SCORE 11 405

```

# Parascore statistics all genes

* PARAzSCORE.genes.tsv (with header, 9 columns)  
        1. gene name  
        2. total para score over all positions  
        3. gene length  
        4. mean para score  
        5. std para score  
        6. median para score  
        7. percentage para score greater than 0 (minimum)  
        8. percentage para score equal 11 (maximum)  
        9. percentage para_zscore greater zero (=conserved)
```
GENE    TOTALPARASCORE  LENGTH  MEAN    STD     MEDIAN  PARASCORE_GREATER_0     PARASCORE_EQUAL_11      PARASCORE_ZSCORE_GREATER_0
A1BG    669     495     1.32    2.49    0       0.34    0.02    0.27
A1CF    2410    602     3.93    3.83    3       0.66    0.09    0.42
```

* PARAzSCORE.families.tsv (with header, 10 columns)  
        1. family id as number  
        2. gene names per family as csv list  
        3. sum of all para scores over all genes within the family  
        4. sum of lengths of all genes within a family  
        5. mean para score over all positions over all genes within the family  
        6. standard deviation para score over all positions over all genes within the family  
        7. median para score over all positions over all genes within the family  
        8. percentage of positions with para score greater than 0 (minimum)  
        9. percentage of positions with para score equal 11 (maximum)  
        10. percentage of positions with para_zscore per family greater than 0    
        
```
FAMILY  GENES   TOTALPARASCORE  TOTALLENGTH     MEAN    STD     MEDIAN  PARASCORE_GREATER_0     PARASCORE_11    PARASCORE_ZSCORE_GREATER_0
2       SNX6,SNX5,SNX32 11199   1225    8.88    2.92    11      0.98    0.52    0.60
3       KLHDC2,HCFC1,KLHDC1,RABEPK,LZTR1,HCFC2,KLHDC3,KLHDC10   11442   5675    1.98    2.89    0       0.43    0.03    0.31
```

* parasubfam.probs.tsv (with header, 7 columns)   
        1. subfamily name  
        2. number of genes per subfamily  
        3. list of gene names  
        4. missing genes  
        5. probability for missense variants   
        6. probability for loss-of-function (lof) variants  
        7. probability for missinse plus lof variants  

```
subfamname      number_of_genes genes   tmissing_genes  p_mis   p_lof   p_mislof
1000.para.1     2       TARBP2,PRKRA            1.911761986731e-05      4.21231818355e-06       2.332993805086e-05
1000.para.2     2       STAU2,STAU1             2.831356316e-05 4.7126497699e-06        3.30262129299e-05
```

