#!/bin/bash

# download annotation files
wget https://zenodo.org/record/817898/files/CCDS_IDS.txt  
wget https://zenodo.org/record/817898/files/CCDS2Ensembl2HGNC.txt  
wget https://zenodo.org/record/817898/files/refSeqEnsCCDS.tsv  

# download para scores

# Para scores per gene
wget https://zenodo.org/record/817898/files/PARAzSCORES.tar.gz  
tar -zxvf PARAzSCORES.tar.gz  
