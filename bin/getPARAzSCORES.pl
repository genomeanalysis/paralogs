#!/usr/bin/env perl 

=head1 NAME

 getPARAzSCORES.pl

=head1 DESCRIPTION

 Calculate paralog scores for a given aminoacid change.

=head1 USAGE
 perl getPARAzSCORES.pl [options]

 OPTIONS:

 Usage:
   Parameter	Description						Values			Default/comment
   -i 		Input file containing AAchanges				FILE		
   -f		Format of input files					ANNOVAR [1]		ANNOVAR (currently only format supported)
   -a		Flag to add more output columns with family info	[01]			0
   -s		Ignore synonymous variants				[01]			0
   -t		Ignore frameshift variants				[01]			0
   -d		Input data directory					Path to input data	../../../data
   -v           Verbose mode
   -h		Print this screen	

   [1] Input formats:
       "ANNOVAR": list of ANNOVAR aachanges (comma separted)  
       HCN1:NM_021072:exon8:c.G2561A:p.R854Q,ENSG00000164588:ENST00000303230:exon8:c.G2561A:p.R854Q

=head1 EXAMPLES

 perl getPARAzSCORES.pl -i example.input
 perl getPARAzSCORES.pl -i example.input -f ANNOVAR
 perl getPARAzSCORES.pl -i example.input -s 1 -a 1
 perl getPARAzSCORES.pl -h

=head1 COPYRIGHT

 Copyright 2017 Luxembourg Centre for Systems Biomedicine

=head1 AUTHOR

 Patrick May <patrick.may@uni.lu>

=cut

use strict;
use File::Basename;
use Cwd;
use Getopt::Std;

################################## USAGE ##################################################
#
#
my $usage=
"\nperl $0 [options] 
 Usage: 
 Parameter    Description                                     	Values                  Default/comment
 -i           Input file containing AAchanges   		FILE            
 -f           Format of input files                           	ANNOVAR [1]             ANNOVAR (currently only format supported)
 -a           Flag to add more output columns with family info  [01]                    1
 -s           Ignore synonymous variants                      	[01]                    0
 -t	      Ignore frameshift variants			[01]			0
 -d           Input data directory                              Path to input data      ../data of the path to this script
 -v           Verbose mode
 -h           Print this screen       
";

my %options=();
my $verbose=0;
my $ignoreSyn=0;
my $ignoreIndel=0;
my $addInfo=0;
my $format="ANNOVAR";
my $abspath=Cwd::abs_path($0);
my $datadir=File::Basename::dirname($abspath)."/../data";

getopts("i:s:t:a:d:vh", \%options);
#check for input
if(defined($options{h})){
	die $usage;
}
if(defined($options{v})){
	$verbose=1;
}
if (!defined $options{i}){
	die "No input given\n$usage";
}
if(defined($options{d})){
	$datadir=$options{d}
}
if(defined($options{f})){
	die "Wrong file format !! Currently only ANNOVAR is supported !\n$usage" if $options{f} ne "ANNOVAR";
}

$ignoreSyn   = $options{s} if(defined $options{s});
$ignoreIndel = $options{t} if(defined $options{t});
$addInfo     = $options{a} if(defined $options{a});

##################################
# read input file
my $aachangefile=$options{i};

my @aachs=();
my @orggenes=();

open(AACHANGE,$aachangefile) or die $!;
while(my $str=<AACHANGE>){
    chomp($str);

    my @curaas=split(/[,;]+/,$str);
    my @curgenes=();
    foreach (@curaas){
    	my($gene,@rest)=split(/\:/,$_);
	push @curgenes,$gene;
    }
    push @orggenes,join(";",@curgenes);
    push @aachs,$str;
}
close(AACHANGE);

my $ccdsids="$datadir/CCDS_IDS.txt";
my %gene2ccds=();
open(CCDS,$ccdsids) or die "# ERROR: $ccdsids file does not exist !!\n";
while(my $str=<CCDS>){
    chomp($str);
    next if $str!~/\S/;
    my ($gene,$ccds)=split(/\t/,$str);
    $gene2ccds{$gene}=$ccds;
}
close(CCDS);

my $hgnc2ensemblfile=$datadir."/CCDS2Ensembl2HGNC.txt";
my %ens2hgnc=();
open(IDS,$hgnc2ensemblfile) or die "# ERROR: $hgnc2ensemblfile file does not exist !!\n";
while(my $str=<IDS>){
    chomp($str);
    next if $str!~/\S/ or $str=~/Ensembl/;
    my ($geneid,$transid,$protid,$hgnc)=split(/\t/,$str);
    next if length($hgnc) eq 0 or length($protid) eq 0;
    if($hgnc){
	$ens2hgnc{$geneid}=$hgnc;
    }
}
close(IDS);

my $refseq2ccds=$datadir."/refSeqEnsCCDS.tsv";
my %ccds2geneids=();
open(IDS,$refseq2ccds) or die "# ERROR: $refseq2ccds file does not exist !!\n";
while(my $str=<IDS>){
    chomp($str);
    next if $str!~/\S/;
    next if $str=~/Transcript/;
    my ($ensgene,$enstrans,$hgnc,$ccds,$refseq)=split(/\t/,$str);
    $ccds2geneids{$ccds}{$ensgene}++;
    $ccds2geneids{$ccds}{$enstrans}++;
    $ccds2geneids{$ccds}{$hgnc}++;
    $ccds2geneids{$ccds}{$ccds}++;
    $ccds2geneids{$ccds}{$refseq}++;
}
close(IDS);

print STDERR "# GENES with CCDS: ".scalar(keys(%gene2ccds))."\n" if $verbose;

# process paragroups
my %groups=();

my $paraloggroupsfile=$datadir."/para_zscores/parasubgroups.detailed.tsv";
open(GROUP,$paraloggroupsfile) or die "# ERROR: $paraloggroupsfile file does not exist !!\n";
while(my $str=<GROUP>){
    chomp($str);
    next if $str!~/\S/;
    my ($famid,$size,$size2,$missing,$genes)=split(/\t/,$str);
    my @genes=split(/,/,$genes);
    foreach my $gene (@genes){
	$groups{$gene}{'para'}=[(scalar(@genes),$genes,$famid)];
    }
}
close(GROUP);

my %genestats=();
my %familystats=();
my %famprobs=();

if ($addInfo){
	my $famprobsfile=$datadir."/para_zscores/parasubfam.probs.tsv";
	open(PROBS,$famprobsfile) or die "# ERROR: $famprobsfile file does not exist !!\n";
	while (my $str=<PROBS>){
    		chomp($str);
    		next if $str!~/\S/ or $str=~/number/;
    		my ($subfamname,$numgenes,$genes,$tmissing_genes,$pmis,$plof,$pmislof)=split(/\t/,$str);
    		my @genes=split(/,/,$genes);
    		foreach my $gene (@genes){
			$famprobs{$gene}=$str;
    		}
	}
	close(PROBS);

	my $paragenestat=$datadir."/para_zscores/PARAzSCORE.genes.tsv";
	open(GENESTAT,$paragenestat) or die "# ERROR: $paragenestat file does not exist !!\n";
	while(my $str=<GENESTAT>){
    		chomp($str);
    		next if $str!~/\S/ or $str=~/TOTAL/;
    		my ($gene,$totalparascore,$len,$mean,$std,$median,$parascoreGreater0,$parascoreEqual11,$zscoreGreater0)=split(/\t/,$str);
    		$genestats{$gene}{'parascore'}=$str;
	}
	close(GENESTAT);

	my $parafamstat=$datadir."/para_zscores/PARAzSCORE.families.tsv";
	open(FAMSTAT,$parafamstat) or die "# ERROR: $parafamstat file does not exist !!\n";
	while(my $str=<FAMSTAT>){
    		chomp($str);
    		next if $str!~/\S/ or $str=~/TOTAL/;
    		my ($family,$genes,$totalparascore,$len,$mean,$std,$median,$parascoreGreater0,$parascoreEqual11,$zscoreGreater0)=split(/\t/,$str);
    		my @genes=split(/,/,$genes);
    		foreach my $gene (@genes){
			$familystats{$gene}=$str;
	    	}
	}
	close(FAMSTAT);
}

my $paralogscoredir=$datadir."/para_zscores/genes";
my $paralogscoreALLids=$datadir."/para_zscores/PARAzSCORE.geneIds.txt";

my %existingGenes=();
open(PARA,$paralogscoreALLids) or die "# ERROR: $paralogscoreALLids file does not exist !!\n";
while(my $str=<PARA>){
    chomp($str);
    next if $str!~/\S/;
    $existingGenes{'para'}{$str}++;
}
close(PARA);

# process the aminoacid change
print "REF_ENS_GENES\tAACHANGE_SCORED\tGENE\tPARASCORE\tPARASCOREzSCORE\tPARAFAM_ID\tPARAFAM_SIZE\tPARAFAM_GENES";

if($addInfo){
    # parasubscore.genes.tsv
    print "\tPARASCORE_GENE\tTOTALPARASCORE\tGENE_LENGTH\tPARASCORE_MEAN\tPARASCORE_STD\tPARASCORE_MEDIAN\tPARASCORE_GREATER_0\tPARASCORE_EQUAL_11\tPARAzSCORE_GREATER_0";
    # parasubscores.subfamilies.tsv
    print "\tSUBFAMILY\tSUBFAMILY_GENES\tSUBFAMILY_TOTALPARASCORE\tSUBFAMILY_TOTALLENGTH\tSUBFAMILY_MEAN\tSUBFAMILY_STD\tSUBFAMILY_MEDIAN\tSUBFAMILY_PARASCORE_GREATER_0\tSUBFAMILY_PARASCORE_11\tSUBFAMILY_PARAzSCORE_GREATER_0";
    # subfam probs
    print "\tSUBFAMILYNAME\tNUMBERofGENESinSUBFAMILY\tSUBFAMILY_GENES2\tMISSING_GENES\tp_mis\tp_lof\tp_mislof";
}
print "\n";

my $ct=0;
foreach my $aach (@aachs){

    my $orggenes=$orggenes[$ct];
    $ct++;
    next if $aach=~/(AAChange.refGene|AAChange.ensGene)/;

    $aach=~s/[\s\t]//g;

    my $ok=0;
    my $toProcess=1;

    print STDERR "\n# GENES: $orggenes NEW AACHANGE: $aach\n" if $verbose;

    if($aach eq 0 or length($aach) eq 0 or $aach !~/\S/){
	print STDERR "# ERROR: LENGTH eq 0: aachange=$aach====\n" if $verbose;
	$toProcess=0;
    }
    my %aachanges=();
    my @aachanges=();
    @aachanges=split(/[\,\;]/,$aach) if $toProcess;
    my $curgene="";

    foreach my $aachange (@aachanges){
	my @split=split(/\:/,$aachange);
        my ($gene,$id,$exon,$cdna,$prot);
	($gene,$id,$exon,$cdna,$prot)=@split if scalar(@split) eq 5;
	if (scalar(@split) eq 2){
	    if($split[1]=~/p\./){
		$gene=$split[0];
		$prot=$split[1];
	    }else{
		die "# ERROR: Wrong format: $aachange\n";
	    }
	}
	$curgene=$gene;
	if ($ignoreIndel and $aachange=~/(del|ins|sub|fs|nfs)/){
	    print STDERR "# SKIP AACHANGE $aachange is INDEL \n" if $verbose;
	    next;
	}

	if ($gene=~/ENS/){
	    if(exists($ens2hgnc{$gene})){
		print STDERR "# CHANGE ENSEMBL TO HGNC: $gene $ens2hgnc{$gene}\n" if $verbose;
		$gene=$ens2hgnc{$gene};
		$curgene=$gene;
	    }else{
		print STDERR "# ENSEMBL ID NOT FOUND: $gene\n" if $verbose;
		next;
	    }
	}
    
	if ($prot =~/p\.([A-Z])(\d+)(.+)$/){
	    if($1 eq $3 and $ignoreSyn){
		print STDERR "# SKIP AACHANGE $aachange is synonymous\n" if $verbose;
		next;
	    }
	    $aachanges{$gene}{$id}=[($1,$2,$3,$aachange)];
	    $ok=1;
	}else{
	    print STDERR "# AACHANGE ---$aachange--- is not MISSENSE\n" if $verbose;
	    next;
	}
    }

    my $found=0;

    foreach my $gene (keys(%aachanges)){

	$curgene=$gene;
	if(exists($gene2ccds{$gene})){
	    my @isoforms=keys(%{$aachanges{$gene}});
	    my $ccds=$gene2ccds{$gene};
	    $ccds=~s/\.\d+$//;
	    print STDERR "# GENE $gene CCDS $ccds found \n" if $verbose;
	    my %geneids=();
	    if(exists($ccds2geneids{$ccds})){
		foreach (keys(%{$ccds2geneids{$ccds}})){
		    $geneids{$_}++;
		}
	    }else{
		print STDERR "# ERROR: no geneids for gene $gene $ccds\n" if $verbose;
		next;
	    }

	    my $parascore="NA";
	    my $parazscore="NA";
	    my $parasize="NA";
	    my $paragenes="NA";
	    my $parafamid="NA";

	    foreach my $isoform (@isoforms){
		if (exists($geneids{$isoform})){
		    print STDERR "# GENE $gene CCDS $ccds ISOFORM $isoform found \n" if $verbose;
		    my ($ref,$pos,$alt,$aachange)=@{$aachanges{$gene}{$isoform}};
		    # paralogs 
		    if (exists($existingGenes{'para'}{$gene})){
			my $genescorefile=$paralogscoredir."/".$gene.".withStats.txt";
			print STDERR "# EXISTS PARA $genescorefile\n" if $verbose; 
			($parascore,$parazscore)=getScore($genescorefile,$ref,$pos);
			if(exists($groups{$gene}{'para'})){
			    $parasize=$groups{$gene}{'para'}[0];
			    $paragenes=$groups{$gene}{'para'}[1];
			    $parafamid=$groups{$gene}{'para'}[2];
			}
			$found=1;
		    }
		
		    $parascore="NA" if $parascore eq "NA";
		    print STDERR "# SCORES FOUND $gene $ccds $isoform $parascore \n" if $found and $verbose;
		    
		    if($found){
			print "$orggenes\t$aachange\t$gene\t$parascore\t$parazscore\t$parafamid\t$parasize\t$paragenes";
			if($addInfo){
			    if(exists($genestats{$gene}{'parascore'})){
				print "\t".$genestats{$gene}{'parascore'};
			    }else{
	    			print "\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA";
			    }
			    if(exists($familystats{$gene})){
				print "\t".$familystats{$gene};
			    }else{
	    			print "\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA";
			    }
			    if(exists($famprobs{$gene})){
				print "\t".$famprobs{$gene};
			    }else{
	    			print "\tNA\tNA\tNA\tNA\tNA\tNA\tNA" 
			    }		      
			}

			print "\n";
		    }
		    last if $found;
		}else{
		    print STDERR "# GENE $gene ISOFORM $isoform NOT given for CCDS: $ccds\n" if $verbose;
		}
	    }
	}else{
	    print STDERR "# ERROR: GENE $gene has no CCDS\n" if $verbose;
	}
	last if $found eq 1;
    }

    if ($found eq 0){
    
	my $genestats="";
	my $familystats="";
	my $familyprobs="";
        
	print STDERR "# ERROR: not found\n" if $verbose;

	my @orggenes=split(/;/,$orggenes);
	foreach my $orggene (@orggenes){
	    print STDERR "# CHECK GENE: $orggene\n" if $verbose;
	    $genestats       = $genestats{$orggene}{'parascore'} if(exists($genestats{$orggene}{'parascore'}));
	    $familystats  = $familystats{$orggene} if exists($familystats{$orggene});
	    $familyprobs  = $famprobs{$orggene} if exists($famprobs{$orggene});
	    last if exists($genestats{$orggene}{'parascore'}) or exists($familystats{$orggene}) or exists($famprobs{$orggene});
	}

	print "$orggenes\t$aach\t$curgene\tNA\tNA\tNA\tNA\tNA";
	#print "$orggenes\t$aach\t$curgene\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA";
	if($addInfo){
	    if ($genestats){
		print "\t$genestats";
	    }else{
		print "\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA";
	    }
	    if ($familystats){
		print "\t$familystats";
	    }else{
		print "\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA";
	    }
	    if($familyprobs){
		print "\t$familyprobs";
	    }else{
		print "\tNA\tNA\tNA\tNA\tNA\tNA\tNA";
	    }	
	} 
	print "\n"; 
    }
}

##############################
# get score from scorefile

sub getScore{
 
    my $genescorefile=$_[0];
    my $ref=$_[1];
    my $pos=$_[2];

    my $score="NA";
    my $medscore="NA";

    open(SCORE,$genescorefile) or die "# ERROR: SCOREFILE: $genescorefile not found\n";
    while(my $str=<SCORE>){
	chomp($str);
	next if $str!~/\S/;
	my ($aapos,$aa,$consscore,$scoreMinusMedian,$scoreMinusMedianStd,$scoreMinusMean,$scoreMinusMeanStd)=split(/\t/,$str);

	if($pos eq $aapos){
	    if($ref eq $aa){
		#$score=$consscore;
		#$score=$scoreMinusMedian;
		$score=$scoreMinusMeanStd;
		return ($consscore,$score);
	    }else{
		print STDERR "# WARNING: at postion $pos the aa $aa is not equal $ref\n" if $verbose;
		return ($consscore,$score);
	    }
	}
    }
    close(SCORE);

    return ($score,$medscore);
}
