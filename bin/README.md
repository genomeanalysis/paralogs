## This directory contains the binary to score amino acid changes with the paralog scores

## Input:
An example input file is given here: [../example/example.input](../example/example.input)

GABRP:NM_014211:exon10:c.1208delT:p.V403fs,ENSG00000094755:ENST00000265294:exon10:c.1208delT:p.V403fs,ENSG00000094755:ENST00000518525:exon11:c.1208delT:p.V403fs  
HCN1:NM_021072:exon8:c.G2561A:p.R854Q,ENSG00000164588:ENST00000303230:exon8:c.G2561A:p.R854Q  
HCN2:NM_001194:exon2:c.T782C:p.L261S,ENSG00000099822:ENST00000251287:exon2:c.T782C:p.L261S


## Run the tool

```
perl getPARAzSCORES.pl -i ../example/example.input  > ../example/example.output

```
 
## Output

An example output file can be found here [../example/example.output](../example/example.output).

```
REF_ENS_GENES	AACHANGE_SCORED	GENE	PARASCORE	PARASCOREzSCORE	PARAFAM_ID	PARAFAM_SIZE	PARAFAM_GENES
GABRP;ENSG00000094755;ENSG00000094755	ENSG00000094755:ENST00000265294:exon10:c.1208delT:p.V403fs	GABRP	2	-0.80	3268.para.1	14	GLRA2,GABRR1,GLRB,GLRA1,GABRB1,GABRD,GABRR3,GABRP,GLRA4,GABRB3,GABRB2,GABRQ,GABRR2,GLRA3
HCN1;ENSG00000164588	HCN1:NM_021072:exon8:c.G2561A:p.R854Q	HCN1	0	-0.87	933.para.1	16	KCNH6,KCNH3,KCNH5,HCN3,KCNH4,HCN2,CNGA2,CNGA1,HCN1,KCNH2,KCNH7,CNGA4,CNGA3,CNGB3,KCNH1,KCNH8
HCN2;ENSG00000099822	ENSG00000099822:ENST00000251287:exon2:c.T782C:p.L261S	HCN2	8	1.66	933.para.1	16	KCNH6,KCNH3,KCNH5,HCN3,KCNH4,HCN2,CNGA2,CNGA1,HCN1,KCNH2,KCNH7,CNGA4,CNGA3,CNGB3,KCNH1,KCNH8

```

* Output format (with -a parameter)

1. REF_ENS_GENES	- genes checked  
2. AACHANGE_SCORED	- aachange that was scored  
3. GENE	- gene for which the aachange was scored  
4. PARASCORE	- para score  
5. PARASCOREzSCORE	- para z-score  
6. PARAFAM_ID - paralog subfamily id  
7. PARAFAM_SIZE	- size of paralog subfamily  
8. PARAFAM_GENES - genes in paralog subfamily	 
